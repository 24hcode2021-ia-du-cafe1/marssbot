import machine
from stm32_alphabot_v2 import AlphaBot_v2
import utime

# init objet
alphabot = AlphaBot_v2()


# init param
duree_tour_complet = 1450
vitesse_tour_complet = 18

utime.sleep(1)

def TournerAngle(angle):
	if (angle > 0):
		alphabot.setMotors(vitesse_tour_complet, -1*vitesse_tour_complet)
		utime.sleep_ms((duree_tour_complet * angle) // 360)
		alphabot.stop()
	elif (angle < 0):
		alphabot.setMotors(-1*vitesse_tour_complet, vitesse_tour_complet)
		utime.sleep_ms((-1 * duree_tour_complet * angle) // 360)
		alphabot.stop()

utime.sleep(1)

TournerAngle(90)

utime.sleep(1)

TournerAngle(-90)

utime.sleep(1)

TournerAngle(120)

utime.sleep(1)

TournerAngle(360)