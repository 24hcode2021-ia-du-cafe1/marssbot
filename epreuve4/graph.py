import matplotlib.pyplot as plt
import networkx as nx

def json2graph(json):
    graph = {}
    for chemin in json["Chemins"]:
        if chemin["a"] not in graph:
            graph[chemin["a"]] = []
        graph[chemin["a"]].append(chemin["b"])

        if chemin["b"] not in graph:
            graph[chemin["b"]] = []
        graph[chemin["b"]].append(chemin["a"])
    return graph

def graph2tree(graph, start, depth=0, previous=None):
    if depth == 0:
        tree = {}
        tree[start] = graph2tree(graph, start, depth + 1)
        return tree
    elif depth == 4:
        return None
    else:
        tree = {}
        for node in graph[start]:
            if previous != node:
                tree[node] = graph2tree(graph, node, depth + 1, start)
        return tree

def tree2paths(tree, origine=None):
    if origine == None:
        return tree2paths(list(tree.values())[0], list(tree.keys())[0])
    if tree == None:
        return [[origine]]
    paths = []
    for key, value in tree.items():
        for path in tree2paths(value, key):
            paths.append([origine] + path)
    return paths

def find_best_paths(paths_red, paths_blue):
    for i in range(len(paths_red[0])):
        if len(paths_red) > 1:
            for path_blue in reversed(paths_blue):
                for path_red in reversed(paths_red):
                    if path_blue[i] in path_red:
                        paths_red.remove(path_red)
        if len(paths_blue) > 1:
            for path_red in paths_red:
                for path_blue in paths_blue:
                    if path_red[i] in path_blue:
                        paths_blue.remove(path_blue)
    return paths_red[0], paths_blue[0]

def display(graph, paths_blue):
    G = nx.DiGraph()
    G.add_nodes_from(graph.keys())
    for key, value in graph.items():
        for node in value:
            G.add_edge(key, node)

    print(G.nodes())
    print(G.edges())

    pos = nx.spring_layout(G)

    color_map = []
    for node in G:
        if node in paths_blue[0]:
            color_map.append('blue')
        else: 
            color_map.append('red')

    nx.draw(G, node_color=color_map, with_labels=True, arrows = False)

    plt.show()

def main(json):
    start_red = 0
    start_blue = 7

    graph = json2graph(json)
    tree_red = graph2tree(graph, start_red)
    tree_blue = graph2tree(graph, start_blue)

    paths_red = tree2paths(tree_red)
    paths_blue = tree2paths(tree_blue)
    
    best_path_red, best_path_blue = find_best_paths(paths_red, paths_blue)

    print("Rouge:", best_path_red)
    print("Bleu:", best_path_blue)

    display(graph, paths_blue)


json = {
    "Chemins":
        [
            {"a": 0, "b": 4},
            {"a": 1, "b": 2},
            {"a": 1, "b": 4},
            {"a": 1, "b": 3},
            {"a": 1, "b": 6},
            {"a": 2, "b": 3},
            {"a": 3, "b": 4},
            {"a": 3, "b": 5},
            {"a": 4, "b": 6},
            {"a": 5, "b": 7}
        ]
}

main(json)

