import machine
from stm32_alphabot_v2 import AlphaBot_v2
from stm32_ssd1306 import SSD1306, SSD1306_I2C
import utime
import bluetooth
from ble_peripheral import DrivePeripheral
from stm32_vl53l0x import VL53L0X
from sudoku.sudoku import Sudoku


def pitch (pin, noteFrequency, noteDuration, silence_ms = 10):
  if noteFrequency is not 0:
    microsecondsPerWave = 1e6 / noteFrequency
    millisecondsPerCycle = 1000 / (microsecondsPerWave * 2)
    loopTime = noteDuration * millisecondsPerCycle
    for x in range(loopTime):
      pin.high()
      utime.sleep_us(int(microsecondsPerWave))
      pin.low()
      utime.sleep_us(int(microsecondsPerWave))
  else:
    utime.sleep_ms(noteDuration)
  utime.sleep_ms(silence_ms)

def TransNote(note):
  result = 0
  extraitNote = note[0:2]
  if extraitNote == "A-":
      result = 440.0
  if extraitNote == "A#":
      result = 466.16
  if extraitNote == "B-":
      result = 493.88
  if extraitNote == "C-":
      result = 261.63
  if extraitNote == "C#":
      result = 277.18
  if extraitNote == "D-":
      result = 293.66
  if extraitNote == "D#":
      result = 311.13
  if extraitNote == "E-":
      result = 329.63
  if extraitNote == "F-":
      result = 349.23
  if extraitNote == "F#":
      result = 369.99
  if extraitNote == "G-":
      result = 392.00
  if extraitNote == "G#":
      result = 415.30
  if extraitNote == "S-":
      result = 0.00
  if note[2:3] == "5":
      result *= 2
  return result

# Musique mystère décodée
def BuzzerMystere(pin, message):

  #pitch(pin, 440, 500)

  tempo = 200
  repos = 40
  compteurSilences = 0
  octave = 100

  note = message[9:14]
  NOTE_LIST = []
  MESSAGE_NOTES = []
  MESSAGE_DURATION = []
  MESSAGE_SLEEP = []
  int 
  for i in range((len(message)-9)//5):
      NOTE_LIST.append(message[i*5+9:(i+1)*5+9])      
  for i in range(len(NOTE_LIST)):
      note = NOTE_LIST[i]
      if (note[0:2] == "S-" and i != 0):
          MESSAGE_SLEEP[i-1-compteurSilences] += tempo * int(note[4:5])
          compteurSilences += 1
      else:
          MESSAGE_SLEEP.append((repos+tempo) * int(note[4:5]))
          MESSAGE_DURATION.append(tempo * int(note[4:5]))
          MESSAGE_NOTES.append(TransNote(note))
  for i in range(len(MESSAGE_NOTES)):
      print(MESSAGE_NOTES[i])
      pitch(pin, MESSAGE_NOTES[i], MESSAGE_DURATION[i], MESSAGE_SLEEP[i])
  print("fin de melody !")

def display_oled(msg):
  oled.fill(0)
  oled.text(f"{msg}", 10, 10)
  oled.show()

def is_position_borne():
  return vl53l0x.getRangeMillimeters() <= 30

def send_msg_ble(msg, dest):
  drv.send_message(recipient=dest, msg=msg)

def ecoute_msg_ble(dest):
  return drv.get_message(recipient=dest)

def demande_instruction_from_pc(etat_robot):
    msg = ecoute_msg_ble('CUSTOM')
    if msg == "START":
      etat_robot = "SUIVI_LIGNE"
      change_etat(etat_robot)
    elif msg == "STOP":
      etat_robot = "STOP"
      change_etat(etat_robot)
    return etat_robot

def resoud_sudoku(msg):
  # ---- Traitement de la trame envoyée par la centrale ---- #

  # On enlève le <SUDOKU>
  puzzleArr = msg[8:].split(",")
  #print(msg[8:])
  puzzleBoard = []

  index = 0
  for i in range(9) :
    puzzleBoard.append([])
    for j in range(9) :
      #print(puzzleArr[index])
      puzzleBoard[i].append(None if puzzleArr[index] == " " else int(puzzleArr[index]))
      index += 1

  # Résolution
  puzzle = Sudoku(3, 3, board=puzzleBoard).solve() 

  # ---- Remodelage de la trame ---- #

  response = puzzle.board

  resolveStr = "<SUDOKU>"
  for i in range(9) :
    # print(','.join(str(v) for v in response[i]))
    resolveStr += ','.join(str(v) for v in response[i])

  return resolveStr

def resoud_mystere(msg):
  alphabot.controlBuzzer(0)
  #BuzzerMystere(d0,msg)
  pitch(pin, 230, 100)
  alphabot.controlBuzzer(1)
  return msg

def resoud_mastermind(msg):

  return msg

def demande_instruction_from_st(etat_robot):
  etat_robot = "ENIGME"
  change_etat(etat_robot)
  msg = ecoute_msg_ble('CUSTOM')
  print(msg[:8])
  #display_oled(msg[:7])
  if msg[:8] == '<SUDOKU>':
    print("...>Enigme sudoku")
    etat_robot = "SUDOKU"
    change_etat(etat_robot)
    rep = resoud_sudoku(msg)
    send_msg_ble(rep,'CUSTOM')
    etat_robot = "ARRET_NOEUD"
    change_etat(etat_robot)
  elif msg[:9] == '<MYSTERE>':
    print("...>Enigme mystere")
    etat_robot = "MYSTERE"
    change_etat(etat_robot)
    rep = resoud_mystere(msg)
    send_msg_ble(rep,'CUSTOM')
    etat_robot = "ARRET_NOEUD"
    change_etat(etat_robot)
  elif msg[:9] == '<MASTERMIND>':
    print("...>Enigme mastermind")
    etat_robot = "MASTERMIND"
    change_etat(etat_robot)
    rep = resoud_mastermind(msg)
    send_msg_ble(rep,'CUSTOM')
    etat_robot = "ARRET_NOEUD"
    change_etat(etat_robot)
  else :
    print(".pas reconnu")
    etat_robot = "ARRET_BORNE"
    change_etat(etat_robot)

  return etat_robot

def change_etat(etat_robot):
  display_oled(etat_robot)
  #send_msg_ble(msg=etat_robot, dest="CUSTOM")

def recuperer_orientation(origine,courante,destination):
  orientation = "0"
  for line in liste_orientation:
    if f"{origine} {courante} {destination}" == line.split(":")[0]:
      orientation = line.split(":")[1]
  return orientation

def TournerAngle(angle):
	if (angle > 0):
		alphabot.setMotors(vitesse_tour_complet, -1*vitesse_tour_complet)
		utime.sleep_ms((duree_tour_complet * angle) // 360)
		alphabot.stop()
	elif (angle < 0):
		alphabot.setMotors(-1*vitesse_tour_complet, vitesse_tour_complet)
		utime.sleep_ms((-1 * duree_tour_complet * angle) // 360)
		alphabot.stop()

def process_orientation(orientation):
  #do stuff orientation
  TournerAngle(int(orientation))
  print(orientation)

def gestion_parcours(origine,courante,destination):
  # on recupere l'orientation, en fonction de la position
  print(f"position {origine},{courante},{destination}")
  orientation = recuperer_orientation(origine,courante,destination)
  print(f"orientation {orientation}")
  process_orientation(orientation)
  robot_position_origine = courante
  robot_position_courante = destination
  liste_reste_parcours.pop(0)
  robot_position_destination = liste_reste_parcours[0]
  print(liste_reste_parcours)
  etat_robot = 'SUIVI_LIGNE'
  return etat_robot

def run_suivi_ligne(etat_robot):
  if is_position_borne():
    alphabot.stop()  
    etat_robot = 'ARRET_BORNE'
    #change_etat(etat_robot)
  else:
    sensorsValue = alphabot.TRSensors_readLine(sensor=0)
    #display_oled(f"IR3 {sensorsValue[2]}")
      
    if sensorsValue[2] < blackLimit : 

      if sensorsValue[1] < blackLimit :
        #display_oled(f"leger L")
        alphabot.turnLeft(vitesseNominale, tempoVirage)
        alphabot.moveForward(vitesseNominale)
        utime.sleep_ms(tempoDroit)

      if sensorsValue[3] < blackLimit :

        #display_oled(f"leger R")
        alphabot.turnLeft(vitesseNominale, tempoVirage)
        alphabot.moveForward(vitesseNominale)
        utime.sleep_ms(tempoDroit)
      else : 
        alphabot.moveForward(vitesseNominale)

    else :
      if sensorsValue[0] < blackLimit or sensorsValue[1] < blackLimit:

        #display_oled(f"fort L")
        alphabot.turnBigLeft(vitesseNominale, tempoVirage)
        #alphabot.moveForward(vitesseNominale)
        #utime.sleep_ms(tempoDroit)
      if sensorsValue[3] < blackLimit or sensorsValue[4] < blackLimit:
        #display_oled(f"fort R")
        alphabot.turnBigRight(vitesseNominale, tempoVirage)
        #alphabot.moveForward(vitesseNominale)
        #utime.sleep_ms(tempoDroit)
  return etat_robot


def isSensorAboveLine(robot, sensorName, blackLimit = 600):
  sensorsValue = robot.TRSensors_readLine(sensor=0) # all sensors values
  if 'IR' in sensorName:
    if sensorName=='IR1' and sensorsValue[0] < blackLimit: return True
    elif sensorName=='IR2' and sensorsValue[1] < blackLimit: return True
    elif sensorName=='IR3' and sensorsValue[2] < blackLimit: return True
    elif sensorName=='IR4' and sensorsValue[3] < blackLimit: return True
    elif sensorName=='IR5' and sensorsValue[4] < blackLimit: return True
    else: return False
  else:
    raise ValueError("name '" + sensorName + "' is not a sensor option")

# -------------------------------
#             MAIN
# -------------------------------



#setup start -> com PC pour start
#boucle tant que pas etat fin
#    check etat 
#    si etat = suivi ligne -> suivi ligne
#    si etat = arret_borne -> gestion enigme / com avec ST
#    si etat = arret_noeud -> geston parcours
#    si etat = arret_obstacle -> c'est de l'attente, tant que obstacle
#    si etat = arret_perdu -> ?
#    si etat = redemarrage -> 

# init objet
alphabot = AlphaBot_v2()
oled = SSD1306_I2C(128, 64, alphabot.i2c, addr=alphabot.getOLEDaddr())
vl53l0x = VL53L0X(i2c=machine.I2C(1))
drv = DrivePeripheral(name='Scolecion-7')
# Buzzer on D0
d0 = machine.Pin('D0', machine.Pin.OUT)

#alphabot.controlBuzzer(0)
#utime.sleep(1)
#alphabot.controlBuzzer(1)

#pitch(d0, 230, 100)

# init param
tempoVirage = 20
tempoDroit = 50
vitesseNominale = 12
blackLimit = 600
duree_tour_complet = 1450
vitesse_tour_complet = 18

etat_robot = 'SETUP'
change_etat(etat_robot)

# SETUP - chargement orientations
liste_orientation = []
with open("OrientationParcours.txt") as fp:
  for line in fp:
    liste_orientation.append(line)

# SETUP - en ecoute du parcours
liste_parcours = ""
while etat_robot == 'SETUP':
  msg = ecoute_msg_ble('CUSTOM')
  if msg[:10] == '<PARCOURS>':
    parcours = msg[10:]
    etat_robot = 'READY'
    change_etat(etat_robot)

liste_parcours = parcours.split(",")

print(liste_parcours)

robot_position_origine = liste_parcours[0]
robot_position_courante = liste_parcours[0]
robot_position_destination = liste_parcours[1]

liste_reste_parcours = liste_parcours

etat_robot = gestion_parcours(robot_position_origine,robot_position_courante,robot_position_destination)


utime.sleep(1)

etat_robot = 'SUIVI_LIGNE'
change_etat(etat_robot)
#etat_robot = demande_instruction_from_pc(etat_robot)

#utime.sleep_ms(1000)

#etat_robot = change_etat('SUIVI_LIGNE')

#while etat_robot != 'STOP':
while len(liste_reste_parcours) > 0:
  
  if etat_robot == 'SUIVI_LIGNE':
    etat_robot = run_suivi_ligne(etat_robot)
  elif etat_robot == 'ARRET_BORNE':
    etat_robot = demande_instruction_from_st(etat_robot)
  elif etat_robot == 'ARRET_NOEUD':
    etat_robot = gestion_parcours(robot_position_origine,robot_position_courante,robot_position_destination)


alphabot.stop()  
