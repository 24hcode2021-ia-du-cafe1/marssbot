import sys
import time
import serial
import threading
import json
import log

log = log.Log(debug=True, name='CWD')
ctrlC = bytes.fromhex("03")
ctrlD = bytes.fromhex("04")

map_id_to_state = {0: 'READY', 1: 'ASK', 2: 'RESULT', 5: 'ASK_CUSTOM', 6: 'RESULT_CUSTOM'}
recipients = ('ST', 'CUSTOM')

# for each function we want to analyze durations, we shall create a variable of type list
# Each duration will be added in this list
# first element contains sum of durations
connectDurations = [0]  # durations to establish a connection
disconnectDurations = [0]  # durations to disconnect
sendDurations = [0]  # durations to send a message
waitForMessageDurations = [0]  # durations waiting for a message


def studyDuration(allMeasures, headline=''):
	"""Decorator to measure duration of a function.
	Add result in allMeasures so we can perform some statistics on this list of measures"""
	def decorator(fct):
		def measureDuration(*args, **kwargs):
			timeStart = time.clock_gettime()
			ret = fct(*args, **kwargs)
			timeEnd = time.clock_gettime()
			delta = int((timeEnd - timeStart) / 1000000)
			allMeasures[0] += delta
			allMeasures.append(delta)
			m = allMeasures[1:]
			log.d(headline, delta, min(m), int(allMeasures[0] / len(m)), max(m))
			return ret
		return measureDuration
	return decorator


class ComWithDongle:
	"""Class to manage communication with dongle, over virtual COM port"""
	def __init__(self, com_port: str, recipient: str, expect_peripheral_name: str = None):
		"""Use 'ST' as recipient to dialog with ST computer.
		Use 'CUSTOM' as recipient for dialogs developed by you and running with your own computer.
		:param com_port: name of com port used by dongle
		:param recipient: one of 'ST' or 'CUSTOM'
		:param expect_peripheral_name: restrict to connect only on peripheral with this name"""
		self.msgId = 0
		if recipient not in recipients:
			exit('bad recipient')
		self.recipient = recipient
		self.expect_peripheral_name = expect_peripheral_name
		self.ble_is_connected = False
		self.peripheral_name = None
		self.peripheral_state = None
		self.received_message = None
		self.mtu_exchanged = False
		self.message_received_flag = threading.Semaphore(0)
		self.ble_end_of_scan_flag = threading.Semaphore(0)
		self.ble_disconnect_flag = threading.Semaphore(0)
		self.mtu_exchanged_flag = threading.Semaphore(0)
		self.write_complete_flag = threading.Semaphore(0)
		try:
			self.ser = serial.Serial(port=com_port, baudrate=115200, timeout=2)
		except serial.SerialException:
			exit(f"no device on port {com_port}")
		self.reset_dongle()
		threading.Thread(name='readOutput', target=self.read_from_com_port, daemon=True).start()

	def reset_dongle(self):
		self.ser.write(ctrlC)
		self.ser.write(ctrlD)
		self.ser.flush()
		log.d('dongle reset', flush=True)
		time.sleep(2)

	def send(self, msg: dict):
		"""Send a message to dongle over COM port
		:param msg: message to send as a dictionary"""
		msg['pcId'] = self.msgId
		self.ser.write(json.dumps(msg).encode("utf-8") + b'\r')
		self.ser.flush()
		log.d('sent:', msg, flush=True)
		self.msgId += 1

	def read_from_com_port(self):
		while 1:
			line = self.ser.readline().rstrip()
			# valid message can't be empty and must start by "{" character
			if type(line) is not bytes or line == b'':
				# empty message received after a timeout on serial connection, to ignore
				continue
			if line[0] != 123:
				# messages not starting by "{" are probably error messages reported by dongle => to display as it can be
				# useful to detect a problem
				log.d('from dongle:', line.decode(), flush=True)
				time.sleep(0.1)
				continue
			try:
				receivedMsg = json.loads(line.decode())
			except json.decoder.JSONDecodeError:
				exit("bad json format: " + line.decode())
			if "dongleId" not in receivedMsg:
				continue
			if receivedMsg["type"] == "connected":
				self.ble_is_connected = True
				self.mtu_exchanged = False
				self.peripheral_name = receivedMsg['name']
				self.peripheral_state = map_id_to_state[receivedMsg['state']]
				log.i(f"BLE '{self.peripheral_name}' connected, state {self.peripheral_state}", flush=True)
				self.ble_end_of_scan_flag.release()
			elif receivedMsg["type"] in ["noPeripheralFound", "connectFailed"]:
				log.d(f"BLE not connected: {receivedMsg['type']}", flush=True)
				self.ble_is_connected = False
				self.peripheral_name = None
				self.peripheral_state = None
				self.ble_end_of_scan_flag.release()
			elif receivedMsg["type"] == 'mtuExchanged':
				self.mtu_exchanged = True
				self.mtu_exchanged_flag.release()
			elif receivedMsg["type"] == 'writeDone':
				self.write_complete_flag.release()
			elif receivedMsg["type"] == "disconnected":
				log.d("BLE disconnected", flush=True)
				self.ble_is_connected = False
				self.mtu_exchanged = False
				self.peripheral_name = None
				self.peripheral_state = None
				self.ble_disconnect_flag.release()
			else:
				self.received_message = receivedMsg
				self.message_received_flag.release()

	#@studyDuration(waitForMessageDurations, headline='receive')
	def get_com_message(self):
		"""Wait a message is received from COM port and return content of this message"""
		self.message_received_flag.acquire(timeout=10)
		if not self.received_message:
			exit("timeout reached while waiting for a message")
		msg = dict(self.received_message)
		self.received_message = None
		return msg

	def wait_end_of_scan(self):
		"""Wait scan is completed and we have received a "connected" or "connectFailed" message"""
		self.ble_end_of_scan_flag.acquire(timeout=3)

	#@studyDuration(connectDurations, headline='connect')
	def connect_or_exit(self, retry: int = 100):
		"""Connect dongle to peripheral over BLE and wait MTU is exchanged
		If we have not succeeded after number of retry defined, stop this program
		:param retry: number of scanAndConnect we can try before to resign"""
		while retry and not self.mtu_exchanged:
			msg = {
				"type": "scanAndConnect", 'recipient': self.recipient,
				'expect_peripheral_name': self.expect_peripheral_name}
			self.send(msg)
			retry -= 1
			self.wait_end_of_scan()
			if self.ble_is_connected:
				# connected, wait MTU is exchanged so connection is ready to send or receive a large message
				self.mtu_exchanged_flag.acquire(timeout=8)
				if not self.mtu_exchanged:
					# connected but failed to exchange MTU => disconnect, connect will be tried again later
					log.w('failed to exchange MTU => reconnect')
					self.send({"type": "disconnect"})
					self.ble_disconnect_flag.acquire(timeout=2)
		if not self.mtu_exchanged:
			exit("unable to connect to peripheral")

	#@studyDuration(connectDurations, headline='connect2')
	def connect(self, retry: int = 100):
		"""Connect dongle to peripheral over BLE and wait MTU is exchanged
		If we have not succeeded after number of retry defined, stop this program
		:param retry: number of scanAndConnect we can try before to resign"""
		while retry and not self.mtu_exchanged:
			msg = {
				"type": "scanAndConnect", 'recipient': self.recipient,
				'expect_peripheral_name': self.expect_peripheral_name}
			self.send(msg)
			retry -= 1
			self.wait_end_of_scan()
			if self.ble_is_connected:
				# connected, wait MTU is exchanged so connection is ready to send or receive a large message
				self.mtu_exchanged_flag.acquire(timeout=8)
				if not self.mtu_exchanged:
					# connected but failed to exchange MTU => disconnect, connect will be tried again later
					log.w('failed to exchange MTU => reconnect')
					self.send({"type": "disconnect"})
					self.ble_disconnect_flag.acquire(timeout=2)

	#@studyDuration(disconnectDurations, headline='disconnect')
	def disconnect_peripheral(self):
		"""Send disconnect message to dongle and wait until we have received the "disconnected" message from dongle"""
		self.send({"type": "disconnect"})
		self.ble_disconnect_flag.acquire(timeout=4)
		# add a small sleep to let peripheral change its state before we start next scan
		time.sleep(0.1)

	#@studyDuration(sendDurations, headline='send')
	def send_question(self, msg: str):
		self.send({"type": "toPeripheral", "msg": msg})
		# wait confirmation message has been received by peripheral
		self.write_complete_flag.acquire(timeout=2)
