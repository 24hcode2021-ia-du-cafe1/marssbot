import sys
from stm32_alphabot_v2 import AlphaBot_v2
import utime
import buzzer

buz = buzzer.Buzzer()

def TransNote(note):
    result = 0
    extraitNote = note[0:2]
    if extraitNote == "A-":
        result = 440.0
    if extraitNote == "A#":
        result = 466.16
    if extraitNote == "B-":
        result = 493.88
    if extraitNote == "C-":
        result = 261.63
    if extraitNote == "C#":
        result = 277.18
    if extraitNote == "D-":
        result = 293.66
    if extraitNote == "D#":
        result = 311.13
    if extraitNote == "E-":
        result = 329.63
    if extraitNote == "F-":
        result = 349.23
    if extraitNote == "F#":
        result = 369.99
    if extraitNote == "G-":
        result = 392.00
    if extraitNote == "G#":
        result = 415.30
    if extraitNote == "S-":
        result = 0.00
    if note[2:3] == "5":
        result *= 2
    return result

# Musique mystère décodée
def BuzzerMystere(robot, message):
    tempo = 180
    repos = 40
    compteurSilences = 0

    note = message[9:14]
    NOTE_LIST = []
    MESSAGE_NOTES = []
    MESSAGE_DURATION = []
    MESSAGE_SLEEP = []
    int 
    for i in range((len(message)-9)//5):
        NOTE_LIST.append(message[i*5+9:(i+1)*5+9])      
    for i in range(len(NOTE_LIST)):
        note = NOTE_LIST[i]
        if (note[0:2] == "S-" and i != 0):
            MESSAGE_SLEEP[i-1-compteurSilences] += tempo * int(note[4:5])
            compteurSilences += 1
        else:
            MESSAGE_SLEEP.append(repos * int(note[4:5]))
            MESSAGE_DURATION.append(tempo * int(note[4:5]))
            MESSAGE_NOTES.append(TransNote(note))
    for i in range(len(MESSAGE_NOTES)):
        buz.pitch(robot, MESSAGE_NOTES[i], MESSAGE_DURATION[i], MESSAGE_SLEEP[i])
    #    print(f"note : {MESSAGE_NOTES[i]}")
    #    print(f"durée : {MESSAGE_DURATION[i]}")
    #    print(f"sleep : {MESSAGE_SLEEP[i]}")


# -------------------------------
# MAIN
# -------------------------------
alphabot = AlphaBot_v2()
utime.sleep(1)

# ------ Bip -------
# Buzzer high: 0
alphabot.controlBuzzer(0)
utime.sleep_us(2000)
# buzzer low: 1
alphabot.controlBuzzer(1)
utime.sleep(5)

message_exemple = "<MYSTERE>D-4:1D-4:1D-4:1G-4:4D#5:4S-1:2C#5:1C-5:1A#4:1A#5:4D#5:2S-1:1C#5:1C-5:1A#4:1A#5:4D#5:2C#5:1C-5:1C#5:1A#4:5"
print('Musique exemple')
BuzzerMystere(alphabot,message_exemple)