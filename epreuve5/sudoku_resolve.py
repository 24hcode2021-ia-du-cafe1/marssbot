from turtle import pu
from sudoku import Sudoku

# ---- Traitement de la trame envoyée par la centrale ---- #

puzzleFile = open("puzzle.txt", 'r')
# On enlève le <SUDOKU>
puzzleArr = puzzleFile.read()[8:].split(",")
puzzleBoard = []

index = 0
for i in range(9) :
    puzzleBoard.append([])
    for j in range(9) :
        puzzleBoard[i].append(None if puzzleArr[index] == " " else int(puzzleArr[index]))
        index += 1

# Résolution
puzzle = Sudoku(3, 3, board=puzzleBoard).solve() 

# ---- Remodelage de la trame ---- #

response = puzzle.board

resolveStr = "<SUDOKU>"
for i in range(9) :
    # print(','.join(str(v) for v in response[i]))
    resolveStr += ','.join(str(v) for v in response[i])
print(resolveStr)

