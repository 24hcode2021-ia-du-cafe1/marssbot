
from doctest import master
from mimetypes import init
import random

class Mastermind :

    def __init__(self): 
        self.colors = "YRGOBP"
        self.answer_possibilities = []

    # Init des 1296 possibilités 
    def reponses_possibles(self, answer_possibilities) :
        arr = self.colors
        for i in range(6):
            for j in range(6):
                for k in range(6):
                    for m in range(6):
                        answer = arr[i] + arr[j] + arr[k] + arr[m]
                        answer_possibilities.append(answer)
        return answer_possibilities
    
    def evaluate(self, guess, answer):

        nbDieses = 0
        nbEtoiles = 0

        # Réponse de l'ordi au robot
        _answer = list(answer[:])

        _guess = list(guess[:])
        evaluation = ""
        for i in range(0, 4):
            if _answer[i] == _guess[i]:
                # Matches exactly, so it's a nbDieses
                nbDieses += 1
                _answer[i] = "X"
                _guess[i] = "Z"
                # Prevent duplicate marking

        for i in range(0, 4):
            for j in range(0, 4):
                if _answer[i] == _guess[j]:
                    nbEtoiles += 1
                    _answer[i] = "X"
                    _guess[j] = "Z"
        while nbDieses > 0:
            evaluation += "#"
            nbDieses -= 1
        while nbEtoiles > 0:
            evaluation += "*"
            nbEtoiles -= 1
        while len(evaluation) < 4:
            evaluation += "-"
        return evaluation

    # Returns our first guess
    def firstTry(self) :
        return ''.join((random.choice(self.colors)) for x in range(4))


    def init_fake_result(self) :
        return ''.join((random.choice(self.colors)) for x in range(4))

    def fake_answer(self, guess, result) :

        nbDieses = 0
        nbEtoiles = 0
        nbTirets = 0
        answer = ""

        for i in range(4): 
            if (guess[i] == result[i]):  
                nbDieses += 1
            else:
                if (guess[i] in result):
                    nbEtoiles += 1
                else:
                    nbTirets += 1

        # Formattage ordonné (##*-)
        for letter in range(nbDieses) :
            answer += "#"
        for letter in range(nbEtoiles) :
            answer += "*"
        for letter in range(nbTirets) :
            answer += "-"

        return answer

    def cull_possibilities(self, guess, evaluation):
        for answer in list(self.answer_possibilities):
            possible_answer_evaluation = self.evaluate(guess, answer)
            if possible_answer_evaluation != evaluation:
                self.answer_possibilities.remove(answer)

    def solution(self) :
        # Réponse fictive de notre connection
        fake_result = self.init_fake_result()
        print("Result  : " + fake_result)
        self.reponses_possibles(self.answer_possibilities) # 1296
        firstGuess = self.firstTry()
        print("First Guess : " + firstGuess)
        answer = self.fake_answer(firstGuess, fake_result)
        print("Answer : " + answer)
        evaluation = answer
        print("Evaluation : " + evaluation)
        print("Avant :")
        print(len(self.answer_possibilities))
        self.cull_possibilities(firstGuess, evaluation)
        print("Apres :")
        print(len(self.answer_possibilities))
        
        i  = 1
        while (len(self.answer_possibilities) > 1) :
            print(f"Iteration n°{i}")
            answer = self.fake_answer(self.answer_possibilities[0], fake_result)
            self.cull_possibilities(self.answer_possibilities[0], answer)
            print(len(self.answer_possibilities))
            i += 1

        return self.answer_possibilities[0]


mastermind = Mastermind()
combinaison_gagnante = mastermind.solution()
print(f"La combo gagnante : {combinaison_gagnante}")
