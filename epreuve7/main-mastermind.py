import random

fake_result="PPPP"
firstGuess="YBPO"
answer_possibilities = []
colors= "YRGOBP"

def reponses_possibles() :
    arr = colors
    for i in range(6):
        for j in range(6):
            for k in range(6):
                for m in range(6):
                    answer = arr[i] + arr[j] + arr[k] + arr[m]
                    answer_possibilities.append(answer)
    return answer_possibilities

def sendTry(guess):
    return evaluate(guess,fake_result)

def evaluate(guess, answer):

        nbDieses = 0
        nbEtoiles = 0

        # Réponse de l'ordi au robot
        _answer = list(answer[:])

        _guess = list(guess[:])
        evaluation = ""
        for i in range(0, 4):
            if _answer[i] == _guess[i]:
                # Matches exactly, so it's a nbDieses
                nbDieses += 1
                _answer[i] = "X"
                _guess[i] = "Z"
                # Prevent duplicate marking
        for i in range(0, 4):
            for j in range(0, 4):
                if _answer[i] == _guess[j]:
                    nbEtoiles += 1
                    _answer[i] = "X"
                    _guess[j] = "Z"
        while nbDieses > 0:
            evaluation += "#"
            nbDieses -= 1
        while nbEtoiles > 0:
            evaluation += "*"
            nbEtoiles -= 1
        while len(evaluation) < 4:
            evaluation += "-"
        return evaluation


def cull_possibilities(guess, answer):
    for possibility in list(answer_possibilities):
        possible_answer_evaluation = evaluate(guess, possibility)
        if possible_answer_evaluation != answer:
            answer_possibilities.remove(possibility)

# Returns our first guess
def firstTry() :
    return ''.join((random.choice(colors)) for x in range(4))

def jouer():
    # liste possibilité 
    # Init des 1296 possibilités 
    answer_possibilities = reponses_possibles()
    answer = sendTry(firstGuess)
    cull_possibilities(firstGuess,answer)
    iteration = 1
    while (len(answer_possibilities) > 1) :
        print(f"Iteration n°{iteration}")
        answer = sendTry(answer_possibilities[0])
        cull_possibilities(answer_possibilities[0], answer)
        iteration += 1
    return answer_possibilities[0]

combinaison_gagnante = jouer()
print(f"La combo gagnante : {combinaison_gagnante}")