import random  
import string  


#def generateRandom(color):
    
 
color = 'YBRGWO' # la liste des couleurs
# define the condition for random string  
result = ''.join((random.choice(color)) for x in range(4))  
print("String généré aléatoirement : ", result)  

n = input("Trouver le code des 4 couleurs parmis la liste [ YBRGWO ]: ")
    
for letter in n:
    if(letter not in color):
        n = input("[Saisie invalide] - Saisir une autre entrée 4 couleurs [ YBRGWO ] : ")

if(len(n) != 4):
    n = input("[Saisie invalide] - Saisir une autre entrée 4 couleurs [ YBRGWO ] : ")

# condition to test equality of the
# guess made. Program terminates if true.
if (n == result):  
    print("Code trouvé du premier coup")
else:
    # ctr variable initialized. It will keep correctdata of 
    # the number of tries the Player takes to guess the number.
    ctr = 0  
    
    # while loop repeats as long as the 
    # Player fails to guess the number correctly.
    while (n != result and ctr < 10):  
        # variable increments every time the loop
        # is executed, giving an idea of how many
        # guesses were made.
        ctr += 1  
  
        correctdata = 0
  
        # explicit type conversion of an integer to
        # a string in order to ease extraction of digits
        #n = str(n)  
  
        # explicit type conversion of a string to an integer
        #result = str(result)  
  
        # correct[] list stores digits which are correct
        #correct = ['-']*4  
        correct = ''
        # for loop runs 4 times le password a 4 digit
        for i in range(0, 4): 
            # check de l'égalité entre digit
            if (n[i] == result[i]):  
                # le nombre de correctdata est incrémenté 
                correctdata += 1  
                # on stocke la valeur de i qui correspond dans le tableau correcte.
                correct += "#"  
            else:
                # Si un caractere saisie est dans le password
                if (n[i] in result):
                    # trouver le premier caractère qui n'est pas #
                    correct += "*"
                else:
                    correct += "-"
                    
        # when not all the digits are guessed correctly.
        if (correctdata < 4):  
            print("Pas encore le bon mot de passe,", correctdata, " couleure(s) correcte(s)!")
            print("Votre résutlat :")
            for k in correct:
                print(k, end=' ')
            print('\n')
            print('\n')
            print("Nombre de tentatives restantes : " 10-ctr)
            n = input("Choississez vos prochaines couleurs : ")
    # condition for equality.
    if n == result:  
        print("You've become a Mastermind!")
        print("It took you only", ctr, "tries.")
