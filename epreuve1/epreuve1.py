import machine
import neopixel
from stm32_alphabot_v2 import AlphaBot_v2
from stm32_ssd1306 import SSD1306, SSD1306_I2C
import utime

# Neopixel on D7
d7 = machine.Pin('D7', machine.Pin.OUT)
alphabot = AlphaBot_v2()
npAlphabot = neopixel.NeoPixel(d7, 4)
oled = SSD1306_I2C(128, 64, alphabot.i2c, addr=alphabot.getOLEDaddr())

def neopixel_showAllLed(neoPx, ledCount, R, G, B):
  for i in range(ledCount):
    neoPx[i] = (R, G, B)
  neoPx.write()

while True:
  neopixel_showAllLed(npAlphabot, 4, 34, 181, 115)
  oled.text('Bonjour', 0, 0)
  oled.show()
  utime.sleep(2)
  oled.fill(0)
  oled.show()
  neopixel_showAllLed(npAlphabot, 4, 51, 102, 255)
