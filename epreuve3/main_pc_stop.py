#!/usr/bin/python3
# To know COM port to use as argument, run following command before and after dongle connection:
# python -m serial.tools.list_ports
# Port in second result but not in first result is port used by dongle.

import sys
import time
import argparse
import random
from ComWithDongle import ComWithDongle


def randStr(length: int = 10):
	"""Generate a random string of characters
	:param length: number of characters"""
	allowedChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	out = ''
	for _ in range(length):
		out += allowedChar[random.randrange(len(allowedChar))]
	return out


parser = argparse.ArgumentParser(description='Script to communicate with STM32WB55 dongle connected on computer')
parser.add_argument('-c', '--com', type=str, help='id of com port used')
args = parser.parse_args()

if not args.com:
	exit('missing argument --com')

com = ComWithDongle(com_port=args.com, recipient='CUSTOM', expect_peripheral_name='Scolecion-7')
#com = ComWithDongle(com_port=args.com, recipient='ST', expect_peripheral_name='Test2')
try:
	com.send({'type': 'testPcToCentral', 'msg': "Hello central, I'm PC"})
	print(com.get_com_message())

	
	while True:
		com.connect_or_exit(retry=1)
		print("connect")
		print("state", com.peripheral_state)
		if com.peripheral_state in ['ASK', 'ASK_CUSTOM']:
			# send message to peripheral
			com.send_question(msg="STOP")
		elif com.peripheral_state in ['RESULT', 'RESULT_CUSTOM']:
			# get message peripheral needs to send
			msg = com.get_com_message()
			print('answer', msg, flush=True)
		else:
			exit(f"bad state {com.peripheral_state}")

		com.disconnect_peripheral()

except KeyboardInterrupt:
	exit(0)
