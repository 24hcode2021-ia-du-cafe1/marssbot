import machine
from stm32_alphabot_v2 import AlphaBot_v2
from stm32_ssd1306 import SSD1306, SSD1306_I2C
import utime
import bluetooth
from ble_peripheral import DrivePeripheral
from stm32_vl53l0x import VL53L0X

def display_oled(msg):
  oled.fill(0)
  oled.text(f"{msg}", 10, 10)
  oled.show()

def is_position_borne():
  return vl53l0x.getRangeMillimeters() <= 30

def send_msg_ble(msg, dest):
  drv.send_message(recipient=dest, msg=msg)

def ecoute_msg_ble(dest):
  return drv.get_message(recipient=dest)

def demande_instruction_from_pc(etat_robot):
    msg = ecoute_msg_ble('CUSTOM')
    if msg == "START":
      etat_robot = "SUIVI_LIGNE"
      change_etat(etat_robot)
    elif msg == "STOP":
      etat_robot = "STOP"
      change_etat(etat_robot)
    return etat_robot

def demande_instruction_from_st(etat_robot):
  etat_robot = "ENIGME"
  change_etat(etat_robot)
  msg = ecoute_msg_ble('ST')
  display_oled(msg)
  #...
  return etat_robot

def change_etat(etat_robot):
  display_oled(etat_robot)
  send_msg_ble(msg=etat_robot, dest="CUSTOM")

def run_suivi_ligne(etat_robot):




  if is_position_borne():
    alphabot.stop()  
    etat_robot = 'ARRET_BORNE'
    #change_etat(etat_robot)
  else:
    sensorsValue = alphabot.TRSensors_readLine(sensor=0)
    #display_oled(f"IR3 {sensorsValue[2]}")
      
    if sensorsValue[2] < blackLimit : 

      if sensorsValue[1] < blackLimit :
        #display_oled(f"leger L")
        alphabot.turnLeft(vitesseNominale, tempoVirage)
        alphabot.moveForward(vitesseNominale)
        utime.sleep_ms(tempoDroit)

      if sensorsValue[3] < blackLimit :

        #display_oled(f"leger R")
        alphabot.turnLeft(vitesseNominale, tempoVirage)
        alphabot.moveForward(vitesseNominale)
        utime.sleep_ms(tempoDroit)
      else : 
        alphabot.moveForward(vitesseNominale)

    else :
      if sensorsValue[0] < blackLimit or sensorsValue[1] < blackLimit:

        #display_oled(f"fort L")
        alphabot.turnBigLeft(vitesseNominale, tempoVirage)
        #alphabot.moveForward(vitesseNominale)
        #utime.sleep_ms(tempoDroit)
      if sensorsValue[3] < blackLimit or sensorsValue[4] < blackLimit:
        #display_oled(f"fort R")
        alphabot.turnBigRight(vitesseNominale, tempoVirage)
        #alphabot.moveForward(vitesseNominale)
        #utime.sleep_ms(tempoDroit)
  return etat_robot


def isSensorAboveLine(robot, sensorName, blackLimit = 600):
  sensorsValue = robot.TRSensors_readLine(sensor=0) # all sensors values
  if 'IR' in sensorName:
    if sensorName=='IR1' and sensorsValue[0] < blackLimit: return True
    elif sensorName=='IR2' and sensorsValue[1] < blackLimit: return True
    elif sensorName=='IR3' and sensorsValue[2] < blackLimit: return True
    elif sensorName=='IR4' and sensorsValue[3] < blackLimit: return True
    elif sensorName=='IR5' and sensorsValue[4] < blackLimit: return True
    else: return False
  else:
    raise ValueError("name '" + sensorName + "' is not a sensor option")

# -------------------------------
#             MAIN
# -------------------------------



#setup start -> com PC pour start
#boucle tant que pas etat fin
#    check etat 
#    si etat = suivi ligne -> suivi ligne
#    si etat = arret_borne -> gestion enigme / com avec ST
#    si etat = arret_noeud -> geston parcours
#    si etat = arret_obstacle -> c'est de l'attente, tant que obstacle
#    si etat = arret_perdu -> ?
#    si etat = redemarrage -> 

# init objet
alphabot = AlphaBot_v2()
oled = SSD1306_I2C(128, 64, alphabot.i2c, addr=alphabot.getOLEDaddr())
vl53l0x = VL53L0X(i2c=machine.I2C(1))
drv = DrivePeripheral(name='Scolecion-7')

# init param
tempoVirage = 20
tempoDroit = 50
vitesseNominale = 12
blackLimit = 600
etat_robot = 'SUIVI_LIGNE'

#change_etat(etat_robot)
#etat_robot = demande_instruction_from_pc(etat_robot)

#utime.sleep_ms(1000)

#etat_robot = change_etat('SUIVI_LIGNE')

while etat_robot != 'STOP':

  
  if etat_robot == 'SUIVI_LIGNE':
    etat_robot = run_suivi_ligne(etat_robot)
  elif etat_robot == 'ARRET_BORNE':
    etat_robot = demande_instruction_from_st(etat_robot)

alphabot.stop()  
