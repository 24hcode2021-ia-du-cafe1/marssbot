import machine
from stm32_alphabot_v3 import AlphaBot_v3
import utime

alphabot = AlphaBot_v3()

def isSensorAboveLine(robot, sensorName, blackLimit = 300):
  sensorsValue = robot.TRSensors_readLine(sensor=0) # all sensors values
  if 'IR' in sensorName:
    if sensorName=='IR1' and sensorsValue[0] < blackLimit: return True
    elif sensorName=='IR2' and sensorsValue[1] < blackLimit: return True
    elif sensorName=='IR3' and sensorsValue[2] < blackLimit: return True
    elif sensorName=='IR4' and sensorsValue[3] < blackLimit: return True
    elif sensorName=='IR5' and sensorsValue[4] < blackLimit: return True
    else: return False
  else:
    raise ValueError("name '" + sensorName + "' is not a sensor option")

tempoVirage = 200
tempoDroit = 500
vitesseNominale = 15


while True:
  if isSensorAboveLine(alphabot, 'IR3') : 
    if isSensorAboveLine(alphabot, 'IR2') :
      alphabot.turnLeft(10, tempoVirage)
      alphabot.moveForward(vitesseNominale)
      utime.sleep_ms(tempoDroit)

    if isSensorAboveLine(alphabot, 'IR4') :
        alphabot.turnLeft(10, tempoVirage)
        alphabot.moveForward(vitesseNominale)
        utime.sleep_ms(tempoDroit)
    else : 
      alphabot.moveForward(vitesseNominale)

  else :
    if isSensorAboveLine(alphabot, 'IR1') or isSensorAboveLine(alphabot, 'IR2') :
      alphabot.turnLeft(30, tempoVirage)
      alphabot.moveForward(vitesseNominale)
      utime.sleep_ms(tempoDroit)
    if isSensorAboveLine(alphabot, 'IR4') or isSensorAboveLine(alphabot, 'IR5') :
      alphabot.turnRight(30, tempoVirage)
      alphabot.moveForward(vitesseNominale)
      utime.sleep_ms(tempoDroit)
    
    # Message d'erreur
    else :
      alphabot.stop()